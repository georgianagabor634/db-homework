package com.example.UnitTestHomework.unit;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.model.Wishlist;
import com.example.UnitTestHomework.service.CartService;
import com.example.UnitTestHomework.service.WishlistService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WishlistServiceTest {
    @Autowired
    private WishlistService wishlistService;
    private Wishlist wishlist;

    @BeforeAll
    public void setUp(){
        wishlist=wishlistService.addWishlist(new Wishlist());
    }

    @Test
    public void givenWishlistId_WhenGetWishlistId_ThenVerifyWishlist(){
        Assertions.assertEquals(wishlist.getId(),wishlistService.getById(1).getId());
    }
}
