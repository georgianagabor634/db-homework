package com.example.UnitTestHomework.unit;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.service.CartService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CartServiceTest {
    @Autowired
    private CartService cartService;
    private Cart cart;

    @BeforeAll
    public void setUp(){
        cart=cartService.addCart(new Cart());
    }

    @Test
    public void givenCartId_WhenGetCartId_ThenVerifyCart(){
       Assertions.assertEquals(cart.getId(),cartService.getById(1).getId());
    }

}
