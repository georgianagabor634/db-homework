package com.example.UnitTestHomework.unit;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.model.User;
import com.example.UnitTestHomework.service.CartService;
import com.example.UnitTestHomework.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;
    private User user;

    @BeforeAll
    public void setUp(){
        user=userService.addUser(new User());
    }

    @Test
    public void givenUsertId_WhenGetUsertId_ThenVerifyUser(){
        Assertions.assertEquals(user.getId(),userService.getById(1).getId());
    }
}
