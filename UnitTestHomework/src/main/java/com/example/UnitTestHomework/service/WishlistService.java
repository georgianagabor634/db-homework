package com.example.UnitTestHomework.service;

import com.example.UnitTestHomework.model.Wishlist;
import com.example.UnitTestHomework.repository.ProductRepository;
import com.example.UnitTestHomework.repository.WishlistRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WishlistService {
    private final WishlistRepository wishlistRepository;
    private final ProductRepository productRepository;

    public Wishlist getById(Integer id){
        return wishlistRepository.findById(id).get();
    }
    public Wishlist addWishlist(Wishlist wishlist){
        return wishlistRepository.save(wishlist);
    }

    public Wishlist updateWishlist(Wishlist wishlist){
        return wishlistRepository.save(wishlist);
    }

    public void deleteWishlist(Integer id){
        wishlistRepository.deleteById(id);
    }

    public Wishlist addProductToWishlist(Integer productId,Integer userId){
        Wishlist wishlist=wishlistRepository.getWishlistByUserId(userId);
        wishlist.addToWishlist(productRepository.findById(productId).get());
        return wishlistRepository.save(wishlist);
    }
    public Wishlist removeProductFromWishlist(Integer productId,Integer userId){
        Wishlist wishlist=wishlistRepository.getWishlistByUserId(userId);
        wishlist.removeFromWishlist(productRepository.findById(productId).get());
        return wishlistRepository.save(wishlist);
    }

    public Wishlist getWishlistByUserId(Integer userId){
        return wishlistRepository.getWishlistByUserId(userId);
    }
}
