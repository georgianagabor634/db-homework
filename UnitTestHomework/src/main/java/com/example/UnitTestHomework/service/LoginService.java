package com.example.UnitTestHomework.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.UnitTestHomework.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class LoginService {
    public ArrayList<String>myList = new ArrayList<>();

    private final UserRepository userRepository;
    public boolean validation(String name,String password){
        return userRepository.findUserByNameAndPassword(name,password).isPresent();
    }
    public String checkLogin(String name, String password){
        if(validation(name,password)){
            Algorithm algorithm = Algorithm.HMAC256(name+"."+password);
            String token = JWT.create()
                    .withIssuer("myAuthService")
                    .withExpiresAt(new Date())
                    .sign(algorithm);
            myList.add(token);
            return token;
        }
        return "";
    }
    public String checkJWT(String token){
        if(myList.contains(token))
            return JWT.decode(token).getPayload();
        return "";
    }
}
