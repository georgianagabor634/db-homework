package com.example.UnitTestHomework.service;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.model.User;
import com.example.UnitTestHomework.model.Wishlist;
import com.example.UnitTestHomework.repository.CartRepository;
import com.example.UnitTestHomework.repository.ProductRepository;
import com.example.UnitTestHomework.repository.UserRepository;
import com.example.UnitTestHomework.repository.WishlistRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    public User getById(Integer id){
        return userRepository.findById(id).get();
    }

    public List<User> getAll(){
        List<User> users=new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(users::add);
        return users;
    }

    public User addUser(User user){
        return userRepository.save(user);
    }

    public User updateUser(User user){
        return userRepository.save(user);
    }

    public void deleteUser(Integer id){
        userRepository.deleteById(id);
    }

    public User addProductToCart(Integer productId,Integer userId){
        User user=userRepository.getById(userId);
        user.addProductToCart(productRepository.getById(productId));
        return userRepository.save(user);
    }

    public User removeProductFromCart(Integer productId,Integer userId){
        User user=userRepository.getById(userId);
        user.removeProductFromCart(productRepository.getById(productId));
        return userRepository.save(user);
    }
    public User addProductToWishlist(Integer productId,Integer userId){
        User user=userRepository.getById(userId);
        user.addProductToWishlist(productRepository.getById(productId));
        return userRepository.save(user);
    }
    public User removeProductFromWishlist(Integer productId,Integer userId){
        User user=userRepository.getById(userId);
        user.removeProductFromWishlist(productRepository.getById(productId));
        return userRepository.save(user);
    }
}
