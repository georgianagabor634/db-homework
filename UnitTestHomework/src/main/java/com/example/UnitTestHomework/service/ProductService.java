package com.example.UnitTestHomework.service;

import com.example.UnitTestHomework.model.Product;
import com.example.UnitTestHomework.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public List<Product> getAll(){
        List<Product> products=new ArrayList<>();
        productRepository.findAll().iterator().forEachRemaining(products::add);
        return products;
    }
    public Product getById(Integer id){
        return productRepository.findById(id).get();
    }

    public Product addProduct(Product product){
        return productRepository.save(product);
    }
    public Product updateProduct(Product product){
        return productRepository.save(product);
    }
    public void deleteProduct(Integer id){
        productRepository.deleteById(id);
    }
}
