package com.example.UnitTestHomework.service;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.repository.CartRepository;
import com.example.UnitTestHomework.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CartService {
    private final CartRepository cartRepository;
    private final ProductRepository productRepository;

    public Cart getById(Integer id){
        return cartRepository.findById(id).get();
    }

    public Cart addCart(Cart cart){
        return cartRepository.save(cart);
    }

    public Cart updateCart(Cart cart){
        return cartRepository.save(cart);
    }

    public void deleteCart(Integer id){
        cartRepository.deleteById(id);
    }

    public Cart addProductToCart(Integer productId,Integer userId){
        Cart cart=cartRepository.getCartByUserId(userId);
        cart.addToCart(productRepository.findById(productId).get());
        return cartRepository.save(cart);
    }
    public Cart removeProductFromCart(Integer productId,Integer userId){
        Cart cart=cartRepository.getCartByUserId(userId);
        cart.removeFromCart(productRepository.findById(productId).get());
        return cartRepository.save(cart);
    }

    public Cart getCartByUserId(Integer userId){
        return cartRepository.getCartByUserId(userId);
    }

}
