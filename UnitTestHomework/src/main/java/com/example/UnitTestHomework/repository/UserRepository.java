package com.example.UnitTestHomework.repository;

import com.example.UnitTestHomework.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    Optional<User> findUserByNameAndPassword(String name, String password);
}
