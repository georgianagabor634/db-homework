package com.example.UnitTestHomework.repository;

import com.example.UnitTestHomework.model.Product;
import com.example.UnitTestHomework.model.Wishlist;
import com.example.UnitTestHomework.service.WishlistService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WishlistRepository extends JpaRepository<Wishlist,Integer> {
    public Wishlist getWishlistByUserId(Integer userId);
}
