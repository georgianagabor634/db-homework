package com.example.UnitTestHomework.repository;

import com.example.UnitTestHomework.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart,Integer> {
    public Cart getCartByUserId(Integer userId);
}
