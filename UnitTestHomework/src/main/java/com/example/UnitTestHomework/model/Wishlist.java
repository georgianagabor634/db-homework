package com.example.UnitTestHomework.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Wishlist {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    @OneToOne
    @JsonBackReference
    private User user;
    @OneToMany
    private List<Product> products;

    public Wishlist(User user){
        this.user=user;
    }

    public void addToWishlist(Product product){
        products.add(product);
    }

    public void removeFromWishlist(Product product){
        products.remove(product);
    }
}
