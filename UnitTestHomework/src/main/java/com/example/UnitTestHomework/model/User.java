package com.example.UnitTestHomework.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private String name;
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    @JsonManagedReference
    private Cart cart=new Cart(this);
    @OneToOne(cascade = CascadeType.ALL)
    @JsonManagedReference
    private Wishlist wishlist=new Wishlist(this);

    public void addProductToCart(Product product){
        cart.addToCart(product);
    }

    public void removeProductFromCart(Product product){
        cart.removeFromCart(product);
    }

    public void addProductToWishlist(Product product){
        wishlist.addToWishlist(product);
    }

    public void removeProductFromWishlist(Product product){
        wishlist.removeFromWishlist(product);
    }
}
