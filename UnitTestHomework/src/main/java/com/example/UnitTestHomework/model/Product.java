package com.example.UnitTestHomework.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Product {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private String name;
    private Double price;
    private Integer quantity;

    @ManyToOne
     private Cart cart;
    @ManyToOne
     private Wishlist wishlist;

}
