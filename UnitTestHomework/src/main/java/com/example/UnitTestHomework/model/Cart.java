package com.example.UnitTestHomework.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer totalProducts;
    @NotNull
    @OneToOne
    @JsonBackReference
     private User user;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Product> products;

    @Override
    public int hashCode() {
        return Objects.hash(id, user, products);
    }

    public Cart(User user){
        this.user=user;
    }
    public void addToCart(Product product)
    {
        products.add(product);
        totalProducts++;
    }

    public void removeFromCart(Product product){
        products.remove(product);
        totalProducts--;
    }
}
