package com.example.UnitTestHomework.controller;

import com.example.UnitTestHomework.model.Product;
import com.example.UnitTestHomework.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping("products")
    public List<Product> getAll(){
        return productService.getAll();
    }
    @GetMapping("product/{id}")
    public Product getProductById(@PathVariable Integer id){
        return productService.getById(id);
    }

    @PostMapping("product/add")
    public Product addProduct(@RequestBody Product product){
        return productService.addProduct(product);
    }
    @PostMapping("product/update")
    public Product updateProduct(@RequestBody Product product){
        return productService.updateProduct(product);
    }

    @DeleteMapping("product/{id}")
    public void deleteProduct(@PathVariable Integer id){
        productService.deleteProduct(id);
    }

}
