package com.example.UnitTestHomework.controller;

import com.example.UnitTestHomework.model.User;
import com.example.UnitTestHomework.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("users")
    public List<User> getAllUsers(){
        return userService.getAll();
    }
    @GetMapping("user/{id}")
    public User getUserById(@PathVariable Integer id){
        return userService.getById(id);
    }
    @PostMapping("user/add")
    public User createUser(@RequestBody User user){
        return userService.addUser(user);
    }
    @PostMapping("user/update")
    public User updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }
    @DeleteMapping("user/{id}")
    public void deleteUser(@PathVariable Integer id){
        userService.deleteUser(id);
    }
    @PostMapping("user/add-to-cart/{productId}/{userId}")
    public User addProductToCart(@PathVariable Integer productId,@PathVariable Integer userId){
        return userService.addProductToCart(productId,userId);
    }
    @PostMapping("user/remove-from-cart/{productId}/{userId}")
    public User removeProductFromCart(@PathVariable Integer productId,@PathVariable Integer userId){
        return userService.removeProductFromCart(productId,userId);
    }
    @PostMapping("user/add-to-wishlist/{productId}/{userId}")
    public User addProductToWishlist(@PathVariable Integer productId,@PathVariable Integer userId){
        return userService.addProductToWishlist(productId,userId);
    }
    @PostMapping("user/remove-from-wishlist/{productId}/{userId}")
    public User removeProductFromWishlist(@PathVariable Integer productId,@PathVariable Integer userId){
        return userService.removeProductFromWishlist(productId,userId);
    }
}
