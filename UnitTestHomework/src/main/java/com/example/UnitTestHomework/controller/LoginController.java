package com.example.UnitTestHomework.controller;

import com.example.UnitTestHomework.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class LoginController {
    private final LoginService loginService;
    @PostMapping("/login")
    public String getJWT(@RequestBody String username, @RequestBody String password){
        return loginService.checkLogin(username,password);
    }
}
