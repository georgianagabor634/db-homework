package com.example.UnitTestHomework.controller;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class CartController {
    private final CartService cartService;

    @GetMapping("cart/id/{id}")
    public Cart getById(@PathVariable Integer id){
        return cartService.getById(id);
    }

    @GetMapping("cart/{userId}")
    public Cart getByUserId(@PathVariable Integer userId){
        return cartService.getCartByUserId(userId);
    }

    @PostMapping("cart/add")
    public Cart addCart(@RequestBody Cart cart){
        return cartService.addCart(cart);
    }
    @PostMapping("cart/update")
    public Cart updateCart(@RequestBody Cart cart){
        return cartService.updateCart(cart);
    }

    @DeleteMapping("cart/{id}")
    public void deleteCart(@PathVariable Integer id){
        cartService.deleteCart(id);
    }

    @PostMapping("cart/add/{productId}/{userId}")
    public Cart addProductToCart(@PathVariable Integer productId,@PathVariable Integer userId){
        return cartService.addProductToCart(productId,userId);
    }

    @PostMapping("cart/remove/{productId}/{userId}")
    public Cart removeProductFromCart(@PathVariable Integer productId,@RequestBody Integer userId){
        return cartService.removeProductFromCart(productId,userId);
    }
}
