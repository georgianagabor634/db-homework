package com.example.UnitTestHomework.controller;

import com.example.UnitTestHomework.model.Cart;
import com.example.UnitTestHomework.model.Wishlist;
import com.example.UnitTestHomework.service.WishlistService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class WishlistController {
    private final WishlistService wishlistService;
    @GetMapping("wishlist/id/{id}")
    public Wishlist getById(@PathVariable Integer id){
        return wishlistService.getById(id);
    }

    @GetMapping("wishlist/{userId}")
    public Wishlist getByUserId(@PathVariable Integer userId){
        return wishlistService.getWishlistByUserId(userId);
    }
    @PostMapping("wishlist/add")
    public Wishlist addWishlist(@RequestBody Wishlist wishlist){
        return wishlistService.addWishlist(wishlist);
    }
    @PostMapping("wishlist/update")
    public Wishlist updateWishlist(@RequestBody Wishlist wishlist){
        return wishlistService.updateWishlist(wishlist);
    }

    @DeleteMapping("wishlist/{id}")
    public void deleteWishlist(@PathVariable Integer id){
        wishlistService.deleteWishlist(id);
    }

    @PostMapping("wishlist/add/{productId}/{userId}")
    public Wishlist addProductToWishlist(@PathVariable Integer productId,@PathVariable Integer userId){
        return wishlistService.addProductToWishlist(productId,userId);
    }

    @PostMapping("wishlist/remove/{productId}/{userId}")
    public Wishlist removeProductFromWishlist(@PathVariable Integer productId,@PathVariable Integer userId){
        return wishlistService.removeProductFromWishlist(productId,userId);
    }
}
